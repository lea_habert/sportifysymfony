<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TrajetRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=TrajetRepository::class)
 */
class Trajet
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresseDepart;

    /**
     * @ORM\OneToMany(targetEntity=Course::class, mappedBy="trajet")
     */
    private $courses;

    /**
     * @ORM\ManyToMany(targetEntity=Repere::class, mappedBy="trajet")
     */
    private $reperes;

    public function __construct()
    {
        $this->courses = new ArrayCollection();
        $this->reperes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getAdresseDepart(): ?string
    {
        return $this->adresseDepart;
    }

    public function setAdresseDepart(?string $adresseDepart): self
    {
        $this->adresseDepart = $adresseDepart;

        return $this;
    }

    /**
     * @return Collection|Course[]
     */
    public function getCourses(): Collection
    {
        return $this->courses;
    }

    public function addCourse(Course $course): self
    {
        if (!$this->courses->contains($course)) {
            $this->courses[] = $course;
            $course->setTrajet($this);
        }

        return $this;
    }

    public function removeCourse(Course $course): self
    {
        if ($this->courses->removeElement($course)) {
            // set the owning side to null (unless already changed)
            if ($course->getTrajet() === $this) {
                $course->setTrajet(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Repere[]
     */
    public function getReperes(): Collection
    {
        return $this->reperes;
    }

    public function addRepere(Repere $repere): self
    {
        if (!$this->reperes->contains($repere)) {
            $this->reperes[] = $repere;
            $repere->addTrajet($this);
        }

        return $this;
    }

    public function removeRepere(Repere $repere): self
    {
        if ($this->reperes->removeElement($repere)) {
            $repere->removeTrajet($this);
        }

        return $this;
    }
}
