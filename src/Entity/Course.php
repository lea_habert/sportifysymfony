<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CourseRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=CourseRepository::class)
 */
class Course
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateCourse;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $heureCourse;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbParticipantsMax;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $difficulte;

    /**
     * @ORM\ManyToOne(targetEntity=Trajet::class, inversedBy="courses")
     */
    private $trajet;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="courses")
     */
    private $createdBy;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateCourse(): ?\DateTimeInterface
    {
        return $this->dateCourse;
    }

    public function setDateCourse(?\DateTimeInterface $dateCourse): self
    {
        $this->dateCourse = $dateCourse;

        return $this;
    }

    public function getHeureCourse(): ?\DateTimeInterface
    {
        return $this->heureCourse;
    }

    public function setHeureCourse(?\DateTimeInterface $heureCourse): self
    {
        $this->heureCourse = $heureCourse;

        return $this;
    }

    public function getNbParticipantsMax(): ?int
    {
        return $this->nbParticipantsMax;
    }

    public function setNbParticipantsMax(?int $nbParticipantsMax): self
    {
        $this->nbParticipantsMax = $nbParticipantsMax;

        return $this;
    }

    public function getDifficulte(): ?string
    {
        return $this->difficulte;
    }

    public function setDifficulte(?string $difficulte): self
    {
        $this->difficulte = $difficulte;

        return $this;
    }

    public function getTrajet(): ?Trajet
    {
        return $this->trajet;
    }

    public function setTrajet(?Trajet $trajet): self
    {
        $this->trajet = $trajet;

        return $this;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }
}
